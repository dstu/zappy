package dstu.zappy

/*
 * Copyright 2012 Donald S. Black.
 * 
 * This file is part of Zappy.
 *
 * Zappy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Zappy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Zappy.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.{util => j}
import com.lambdaworks.jacks.JacksMapper
import scala.collection.JavaConversions._

case class SearchResponse(statusCode: Int,
  currentResultCount: Option[Int],
  totalResultCount: Option[Int],
  limit: Option[Int],
  currentPage: Option[Int],
  pageCount: Option[Int],
  term: String,
  originalTerm: String,
  results: j.List[SearchResult])

case class SearchResult(styleId: Int,
  productId: Int,
  colorId: String,
  brandName: String,
  productUrl: String,
  thumbnailImageUrl: String,
  price: String,
  productName: String,
  originalPrice: String,
  percentOff: String,
  description: String,
  videoUrl: String,
  videoFileName: String,
  videoUploadedDate: String,
  productRating: String,
  brandId: String,
  categoryFacet: String,
  heelHeight: j.List[String],
  subCategoryFacet: String,
  gender: j.List[String])

/**
 * A search action with the Zappos API. An API key and search
 * term must be specified. Other parameters may be specified by
 * setter methods.
 *
 * Instances of this class are immutable; setter methods create
 * new instances.
 */
case class Search(key: String,
  term: String,
  includes: Set[String] = Set.empty,
  excludes: Set[String] = Set.empty,
  facets: Set[String] = Set.empty,
  facetFilters: Map[String, Set[String]] = Map.empty,
  limit: Option[Int] = None,
  page: Option[Int] = None) extends BaseAction[SearchResponse]("Search", key) {
  private def include(item: String) = copy(includes = includes + item, excludes = excludes - item)
  private def exclude(item: String) = copy(includes = includes - item, excludes = excludes + item)

  def this(key: String, term: String) = this(key, term, Set.empty, Set.empty, Set.empty, Map.empty, None, None)

  def withSearchTerm(s: String) = copy(term = s)

  /**
   * Sets the maximum number of items to return.
   * @param i the desired maximum number of items
   * @return a new Search object with the item limit set to the desired value
   */
  def withLimit(i: Int) = copy(limit = Some(i))
  /**
   * Unsets the maximum number of items to return. (The default behavior of the Zappos API will be used instead.)
   * @return a new Search object with the item limit unset
   */
  def withoutLimit = copy(limit = None)

  /**
   * Sets the page of results to return.
   * @param i the desired page
   * @return a new Search object with the page set to the desired value
   */
  def withPage(i: Int) = copy(page = Some(i))
  /**
   * Unsets the page to return. (The default behavior of the Zappos API will be used instead.)
   * @return a new Search object with the page unset
   */
  def withoutPage = copy(page = None)

  /**
   * Adds a facet filter, filtering for items that have one of
   * the given values in the given facet. This replaces any
   * existing filter on the given facet. Multiple filters may be
   * specified; they are logically AND'ed by the Zappos API.
   *
   * @param facet the facet to filter on
   * @param values the values to filter for
   * @return a new Search object with the new facet filter
   */
  def withFacetFilter(facet: String, values: Set[String]) = copy(facetFilters = facetFilters + Pair(facet, values))
  /**
   * Removes all filters on the given facet.
   * @param facet the facet for which to remove filters
   * @return a new Search object without the given facet filter
   */
  def withoutFacetFilter(facet: String) = copy(facetFilters = facetFilters - facet)
  /**
   * Removes all facet filters.
   * @return a new Search object without any facet filters
   */
  def withoutFacetFilters = copy(facetFilters = Map.empty)

  def withStyleId = include("styleId")
  def withoutStyleId = exclude("styleId")
  def withProductId = include("productId")
  def withoutProductId = exclude("productId")
  def withColorId = include("colorId")
  def withoutColorId = exclude("colorId")
  def withBrandName = include("withBrandName")
  def withoutBrandName = exclude("withBrandName")
  def withProductName = include("productName")
  def withoutProductName = exclude("productName")
  def withProductUrl = include("productUrl")
  def withoutProductUrl = exclude("productUrl")
  def withThumbnailImageUrl = include("thumbnailImageUrl")
  def withoutThumbnailImageUrl = exclude("thumbnailImageUrl")
  def withPrice = include("price")
  def withoutPrice = exclude("price")
  def withOriginalPrice = include("originalPrice")
  def withoutOriginalPrice = exclude("originalPrice")
  def withPercentOff = include("percentOff")
  def withoutPercentOff = exclude("percentOff")
  def withDescription = include("description")
  def withoutDescription = exclude("description")
  def withVideoUrl = include("videoUrl")
  def withoutVideoUrl = exclude("videoUrl")
  def withVideoFileName = include("videoFileName")
  def withoutVideoFileName = exclude("videoFileName")
  def withVideoUploadedDate = include("videoUploadedDate")
  def withoutVideoUploadedDate = exclude("videoUploadedDate")
  def withProductRating = include("productRating")
  def withoutProductRating = exclude("productRating")
  def withBrandId = include("brandId")
  def withoutBrandId = exclude("brandId")
  def withCategoryFacet = include("categoryFacet")
  def withoutCategoryFacet = exclude("categoryFacet")
  def withHeelHeight = include("heelHeight")
  def withoutHeelHeight = exclude("heelHeight")
  def withSubCategoryFacet = include("subCategoryFacet")
  def withoutSubCategoryFacet = exclude("subCategoryFacet")
  def withGender = include("txAttrFacet_Gender")
  def withoutGender = exclude("txAttrFacet_Gender")
  def withTotalResultCount = include("totalResultCount")
  def withoutTotalResultCount = exclude("totalResultCount")
  def withFacets = include("facets")
  def withoutFacets = exclude("facets")

  override def parameters =
    Map("term" -> term) ++
      (if (includes.isEmpty) Map.empty else Map("includes" -> JacksMapper.writeValueAsString(includes))) ++
      (if (excludes.isEmpty) Map.empty else Map("excludes" -> JacksMapper.writeValueAsString(excludes))) ++
      (if (facets.isEmpty) Map.empty else Map("facets" -> JacksMapper.writeValueAsString(facets))) ++
      (if (facetFilters.isEmpty) Map.empty else Map("filters" -> JacksMapper.writeValueAsString(facetFilters))) ++
      limit.map(i => Pair("limit", i.toString)).toMap ++
      page.map(i => Pair("page", i.toString)).toMap

  def get = super.get()
}
