package dstu.zappy

/*
 * Copyright 2012 Donald S. Black.
 * 
 * This file is part of Zappy.
 *
 * Zappy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Zappy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Zappy.  If not, see <http://www.gnu.org/licenses/>.
 */

import com.lambdaworks.jacks.JacksMapper
import com.sun.jersey.api.client.Client
import com.sun.jersey.core.util.MultivaluedMapImpl
import javax.ws.rs.core.{ MediaType, MultivaluedMap }

/**
 * Singleton container for API resources.
 */
object Zappos {
  val BASE_URL = "http://api.zappos.com/"
  lazy val client = Client.create
  lazy val resource = client.resource(BASE_URL)
}

/**
 * Parent class of all API actions.
 *
 * @constructor builds a new action
 * @param path the additional path component for the specific action (like "Search" or "Product"). This corresponds to an action in the main Zappos API.
 * @param key the API key to send with the action
 */
abstract class BaseAction[T](path: String, key: String) {
  type Response = T

  /**
   * Provides type conversion from the Scala Map trait to the Jersey MultivaluedMap interface.
   */
  private implicit def mapAsMultivaluedMap(m: Map[String, String]): MultivaluedMap[String, String] =
    m.foldLeft(new MultivaluedMapImpl)((result, entry) => {
      result.add(entry._1, entry._2)
      result
    })

  /**
   * The parameter map for the action. This specifies the
   * parameters of the GET request that the action will
   * send. Subclasses should override this method and fill in
   * their own fields.
   *
   * @return the parameters for the action's GET request
   */
  def parameters: Map[String, String]

  /**
   * Hydrates a Response object from raw JSON. Subclasses may
   * override this method and fill in their own implementation.
   * @param json the raw JSON
   * @return a newly hydrated Response object
   */
  def decodeResponse(s: String)(implicit m: Manifest[Response]): Response = JacksMapper.readValue[Response](s)

  /**
   * Makes the call to the Zappos API.
   * @return the raw JSON of the API response
   */
  def getJson(): String =
    Zappos.resource.path(path).queryParams(parameters + Pair("key", key)).accept(MediaType.APPLICATION_JSON_TYPE).get(classOf[java.lang.String])

  /**
   * Makes the call to the Zappos API and decodes it.
   * @return the response, decoded
   */
  def get()(implicit m: Manifest[Response]): Response = decodeResponse(getJson)
}
