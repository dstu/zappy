package dstu.zappy

/*
 * Copyright 2012 Donald S. Black.
 * 
 * This file is part of Zappy.
 *
 * Zappy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Zappy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Zappy.  If not, see <http://www.gnu.org/licenses/>.
 */

import com.google.common.base.Preconditions.checkState
import com.lambdaworks.jacks.JacksMapper
import java.{util => j}
import scala.collection.JavaConversions._

case class ProductResponse(statusCode: Int,
  product: j.List[ProductResult])

case class ProductResult(productId: Int,
  brandName: String,
  brandId: Option[Int],
  productName: String,
  defaultProductUrl: String,
  defaultPrettyProductUrl: String,
  defaultImageUrl: String,
  description: String,
  gender: String,
  weight: String,
  videos: j.List[VideoInfo],
  videoUrl: String,
  productRating: Option[Int],
  overallRating: Map[String,Int],
  comfortRating: Map[String,Int],
  lookRating: j.Map[String, Int],
  styles: j.List[StyleInfo],
  defaultProductType: String,
  defaultCategory: String,
  defaultSubCategory: String,
  attributeFacetFields: j.Map[String, String])

case class VideoInfo(id: String, filename: String, productId: String, uploadedDate: String, videoEncodingId: String, videoEncodingExtension: String)
case class StyleInfo(styleId: String, color: String, originalPrice: String, price: String, productUrl: String, imageUrl: String, stocks: j.List[j.Map[String, String]])

/**
 * A product query action with the Zappos API. An API key and
 * at least one product ID must be specified. Other parameters may be
 * specified by setter methods.
 *
 * Instances of this class are immutable; setter methods create
 * new instances.
 */
case class Product(key: String,
  productIds: Set[Int] = Set.empty,
  stockIds: Set[String] = Set.empty,
  upcs: Set[String] = Set.empty,
  includes: Set[String] = Set.empty,
  excludes: Set[String] = Set.empty) extends BaseAction[ProductResponse]("Product", key) {
  private def include(item: String) = copy(includes = includes + item, excludes = excludes - item)
  private def exclude(item: String) = copy(includes = includes - item, excludes = excludes + item)

  def this(key: String, productId: Int) = this(key, Set(productId), Set.empty, Set.empty, Set.empty, Set.empty)

  def withProductIds(ids: Int*) = copy(productIds = ids.toSet)
  def withProductIds(s: j.Set[Int]) = copy(productIds = s.toSet)
  def withStockIds(ids: String*) = copy(stockIds = ids.toSet)
  def withStockIds(s: j.Set[String]) = copy(stockIds = s.toSet)
  def withUpcs(ids: String*) = copy(upcs = ids.toSet)
  def withUpcs(s: j.Set[String]) = copy(upcs = s.toSet)

  def withProductId = include("productId")
  def withoutProductId = exclude("productId")
  def withBrandName = include("brandName")
  def withoutBrandName = exclude("brandName")
  def withProductName = include("productName")
  def withoutProductName = exclude("productName")
  def withDefaultProductUrl = include("defaultProductUrl")
  def withoutDefaultProductUrl = exclude("defaultProductUrl")
  def withDefaultPrettyProductUrl = include("defaultPrettyProductUrl")
  def withoutDefaultPrettyProductUrl = exclude("defaultPrettyProductUrl")
  def withDefaultImageUrl = include("defaultImageUrl")
  def withoutDefaultImageUrl = exclude("defaultImageUrl")
  def withDescription = include("description")
  def withoutDescription = exclude("description")
  def withGender = include("gender")
  def withoutGender = exclude("gender")
  def withWeight = include("weight")
  def withoutWeight = exclude("weight")
  def withVideos = include("videos")
  def withoutVideos = exclude("videos")
  def withVideoFileName = include("videoFileName")
  def withoutVideoFileName = exclude("videoFileName")
  def withVideoUrl = include("videoUrl")
  def withoutVideoUrl = exclude("videoUrl")
  def withVideoUploadedDate = include("videoUploadedDate")
  def withoutVideoUploadedDate = exclude("videoUploadedDate")
  def withProductRating = include("productRating")
  def withoutProductRating = exclude("productRating")
  def withOverallRating = include("overallRating")
  def withoutOverallRating = exclude("overallRating")
  def withComfortRating = include("comfortRating")
  def withoutComfortRating = exclude("comfortRating")
  def withLookRating = include("lookRating")
  def withoutLookRating = exclude("lookRating")
  def withStyles = include("styles")
  def withoutStyles = exclude("styles")
  def withDefaultCategory = include("defaultCategory")
  def withoutDefaultCategory = exclude("defaultCategory")
  def withDefaultSubCategory = include("defaultSubCategory")
  def withoutDefaultSubCategory = exclude("defaultSubCategory")
  def withAttributeFacetFields = include("attributeFacetFields")
  def withoutAttributeFacetFields = exclude("attributeFacetFields")

  override def parameters = {
    val nonEmpty = List(Pair("id", productIds), Pair("stockId", stockIds), Pair("upc", upcs)).filter(!_._2.isEmpty)
    checkState(nonEmpty.size == 1, "Must specify only one of product ID, stock ID, or UPC": Any)
    val searchField = nonEmpty.head._1
    val searchValues = nonEmpty.head._2
    checkState(searchValues.size > 0, "Must specify at least one search value": Any)
    checkState(searchValues.size <= 10, "Can't specify more than 10 search values": Any)

    Map(searchField -> JacksMapper.writeValueAsString(searchValues.map(_.toString))) ++
      (if (includes.isEmpty) Map.empty else Map("includes" -> JacksMapper.writeValueAsString(includes))) ++
      (if (excludes.isEmpty) Map.empty else Map("excludes" -> JacksMapper.writeValueAsString(excludes)))
  }

  def get = super.get()
}
