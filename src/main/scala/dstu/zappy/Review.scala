package dstu.zappy

/*
 * Copyright 2012 Donald S. Black.
 * 
 * This file is part of Zappy.
 *
 * Zappy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Zappy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Zappy.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.{util => j}
import scala.collection.JavaConversions._

case class ReviewResponse(statusCode: Int,
  page: Option[Int],
  offset: Option[Int],
  reviews: j.List[ReviewResult])

case class ReviewResult(id: String,
  date: String,
  name: String,
  location: String,
  otherShoes: String,
  summary: String,
  shoeSize: String,
  shoeWidth: String,
  shoeArch: String,
  overallRating: Option[Int],
  comfortRating: Option[Int],
  lookRating: Option[Int])

case class Review(key: String,
  productId: Int,
  page: Option[Int] = None,
  startId: Option[Int] = None) extends BaseAction[ReviewResponse]("Review", key) {
  def this(key: String, productId: Int) = this(key, productId, None, None)

  def withPage(i: Int) = copy(page = Some(i))
  def withoutPage = copy(page = None)
  def withStartId(i: Int) = copy(startId = Some(i))
  def withoutStartId = copy(startId = None)

  override def parameters =
    Map("productId" -> productId.toString) ++
      page.map(i => Pair("page", i.toString)).toMap ++
      startId.map(i => Pair("startId", i.toString)).toMap

  def get = super.get()
}
