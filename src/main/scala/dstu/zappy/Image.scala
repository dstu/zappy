package dstu.zappy

/*
 * Copyright 2012 Donald S. Black.
 * 
 * This file is part of Zappy.
 *
 * Zappy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Zappy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Zappy.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.{util => j}
import com.lambdaworks.jacks.JacksMapper
import scala.collection.JavaConversions._

case class ImageResponse(statusCode: Int,
  productId: Option[Int],
  images: j.Map[String, j.List[ImageResult]])

case class ImageResult(styleId: String,
  productId: Option[Int],
  `type`: String,
  recipeName: String,
  format: String,
  filename: String,
  colorId: String,
  width: Option[Int],
  height: Option[Int],
  uploadDate: String,
  isHighResolution: Option[Boolean],
  tiles: String)

/**
 * An image query. An API key and product ID must be
 * specified. Other parameters may be specified by setter
 * methods.
 *
 * Instances of this class are immutable; setter methods create
 * new instances.
 */
case class Image(key: String,
  productId: Int,
  includes: Set[String] = Set.empty,
  excludes: Set[String] = Set.empty,
  types: Set[String] = Set.empty,
  recipes: Set[String] = Set.empty) extends BaseAction[ImageResponse]("Image", key) {
  private def include(item: String) = copy(includes = includes + item, excludes = excludes - item)
  private def exclude(item: String) = copy(includes = includes - item, excludes = excludes + item)

  def this(key: String, productId: Int) = this(key, productId, Set.empty, Set.empty, Set.empty, Set.empty)

  def withTypes(ts: j.Set[String]) = copy(types = ts.toSet)
  def withoutTypes = copy(types = Set.empty)

  def withRecipes(rs: j.Set[String]) = copy(recipes = rs.toSet)
  def withoutRecipes = copy(recipes = Set.empty)

  def withStyleId = include("styleId")
  def withoutStyleId = exclude("styleId")
  def withProductId = include("productId")
  def withoutProductId = exclude("productId")
  def withType = include("type")
  def withoutType = exclude("type")
  def withRecipeName = include("recipeName")
  def withoutRecipeName = exclude("recipeName")
  def withFormat = include("format")
  def withoutFormat = exclude("format")
  def withFilename = include("filename")
  def withoutFilename = exclude("filename")
  def withColorId = include("colorId")
  def withoutColorId = exclude("colorId")
  def withWidth = include("width")
  def withoutWidth = exclude("width")
  def withHeight = include("height")
  def withoutHeight = exclude("height")
  def withUploadDate = include("uploadDate")
  def withoutUploadDate = exclude("uploadDate")
  def withHighResolution = include("isHighResolution")
  def withoutHighResolution = exclude("isHighResolution")
  def withTiles = include("tiles")
  def withoutTiles = exclude("tiles")

  override def parameters =
    Map("productId" -> productId.toString) ++
      (if (includes.isEmpty) Map.empty else Map("includes" -> JacksMapper.writeValueAsString(includes))) ++
      (if (excludes.isEmpty) Map.empty else Map("excludes" -> JacksMapper.writeValueAsString(excludes)))

  def get = super.get()
}
