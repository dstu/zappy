package dstu.zappy

/*
 * Copyright 2012 Donald S. Black.
 * 
 * This file is part of Zappy.
 *
 * Zappy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Zappy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Zappy.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import javax.ws.rs.core.MediaType
import scala.collection.JavaConversions._

@RunWith(classOf[JUnitRunner])
class ZapposApiSuite extends FunSuite {
  test("Can connect to Zappos API") {
    val textResponse = Zappos.resource.accept(MediaType.APPLICATION_JSON_TYPE).get(classOf[java.lang.String])

    assert(textResponse === """{"welcomeMessage":"Welcome to the Zappos Public API! We're excited to have you here! Give us a spin. We don't bite","developerPortalUrl":"http:\/\/developer.zappos.com\/","gettingStartedUrl":"http:\/\/developer.zappos.com\/content\/getting-started","faqUrl":"http:\/\/developer.zappos.com\/faq","contactUs":{"email":"api@zappos.com","twitter":"http:\/\/www.twitter.com\/zappos_api","mailingList":"http:\/\/groups.google.com\/group\/zappos_api"},"apis":[{"name":"Search","description":"Perform a real time search against our totally impressive product catalog. Not just shoes! Anyone need a kettle or some lipstick?","documentation":"http:\/\/developer.zappos.com\/docs\/api-search-api","example":"http:\/\/api.zappos.com\/Search?term=boots&key=YOUR_KEY"},{"name":"Product","description":"Pull full product details. It's a bundle of information and joy.","documentation":"http:\/\/developer.zappos.com\/docs\/api-product","example":"http:\/\/api.zappos.com\/Product\/7324204?include=[\"styles\"]&key=YOUR_KEY"},{"name":"Image","description":"Get a robust set of images for all of our products. Did you know we shoot our own images here at Zappos? It's always hunting season here.","documentation":"http:\/\/developer.zappos.com\/docs\/api-image","example":"http:\/\/api.zappos.com\/Image?productId=7324204?&key=YOUR_KEY"},{"name":"Statistics","description":"Our fun api! Get cool stats about Zappos purchases.","documentation":"http:\/\/developer.zappos.com\/docs\/api-statistics","example":"http:\/\/api.zappos.com\/Statistics?type=latestStyles&key=YOUR_KEY"},{"name":"Brand","description":"Did you know we carry almost 1000 brands? Get more details on specific brands with this API.","documentation":"http:\/\/developer.zappos.com\/docs\/api-brand","example":"http:\/\/api.zappos.com\/Brand\/101?key=YOUR_KEY"},{"name":"AutoComplete","description":"The AutoComplete API will let you implement search auto-complete on your own site.","documentation":"http:\/\/developer.zappos.com\/docs\/api-auto-complete","example":"http:\/\/api.zappos.com\/AutoComplete?term=ni&key=YOUR_KEY"},{"name":"CoreValue","description":"Nothing screams Zappos like our Core Values.","documentation":"http:\/\/developer.zappos.com\/docs\/api-corevalue","example":"http:\/\/api.zappos.com\/CoreValue"},{"name":"Similarity","description":"Want to find out what products look or are similar to a given style? Now you can!","documentation":"http:\/\/developer.zappos.com\/docs\/api-search-similarity","example":"http:\/\/api.zappos.com\/Search\/Similarity?type=moreLikeThis&limit=5&styleId=14079&emphasis=color&key=YOUR_KEY"}]}""")
  }

  lazy val search = Search(ApiKey.value, "wedge").withLimit(1)
  lazy val searchResponse = search.get()
  lazy val productId = searchResponse.results.head.productId
  lazy val product = Product(ApiKey.value, Set(productId))
  lazy val image = Image(ApiKey.value, productId)
  lazy val review = Review(ApiKey.value, productId)

  test("Can make a search query") {
    assert(searchResponse.results.size === 1)
  }

  test("Can search with all includes") {
    search.withStyleId.withProductId.withColorId.withBrandName.withProductName.withProductUrl.withThumbnailImageUrl.withPrice.withOriginalPrice.withPercentOff.withDescription.withVideoUrl.withVideoFileName.withVideoUploadedDate.withProductRating.withBrandId.withCategoryFacet.withHeelHeight.withSubCategoryFacet.withGender.withTotalResultCount.withFacets.get()
  }

  test("Can make an image query") {
    image.get()
  }

  test("Can make a product query") {
    product.get()
  }

  test("Can make a product query with all includes") {
    product.withProductId.withBrandName.withProductName.withDefaultProductUrl.withDefaultPrettyProductUrl.withDefaultImageUrl.withDescription.withGender.withWeight.withVideos.withVideoFileName.withVideoUrl.withVideoUploadedDate.withProductRating.withOverallRating.withComfortRating.withLookRating.withStyles.withDefaultCategory.withDefaultSubCategory.withAttributeFacetFields.get()
  }

  test("Can make a product query over multiple product IDs") {
    product.withProductIds(productId, productId + 1).get()
  }

  test("Can make a product query over one UPC") {
    product.withProductIds(Set[Int]()).withUpcs(Set("604993700970")).get()
  }

  test("Can make a product query over one stock ID") {
    product.withProductIds(Set[Int]()).withStockIds(Set("10052110")).get()
  }

  test("Can't make a product query with multiple query parameter types") {
    intercept [IllegalStateException] {
      product.withStockIds(Set("10052110","10052111")).withUpcs(Set("613859178419","604993700970")).get()
    }
  }

  test("Can make a product video query") {
    product.withVideos.get()
  }

  test("Can make a review query") {
    review.get()
  }
}
