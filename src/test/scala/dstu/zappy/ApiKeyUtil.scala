package dstu.zappy

/*
 * Copyright 2012 Donald S. Black.
 * 
 * This file is part of Zappy.
 *
 * Zappy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Zappy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Zappy.  If not, see <http://www.gnu.org/licenses/>.
 */

import com.google.common.io.Resources
import java.io.Reader
import java.nio.charset.Charset
import java.util.Properties

object ApiKey {
  val PROPERTIES_FILE_PATH = "apikey.properties"
  val APIKEY_KEY = "zappy.apikey"

  private def readValue: String = {
    val url = Resources.getResource(PROPERTIES_FILE_PATH)
    val inSupplier = Resources.newReaderSupplier(url, Charset.forName("UTF-8"))
    val properties = new Properties
    var in: Reader = null
    try {
      in = inSupplier.getInput
      properties.load(in)
    } finally {
      if (null != in) {
        in.close
        in = null
      }
    }

    properties.getProperty(APIKEY_KEY)
  }

  lazy val value = readValue
}
