package dstu.zappy

/*
 * Copyright 2012 Donald S. Black.
 * 
 * This file is part of Zappy.
 *
 * Zappy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Zappy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Zappy.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ApiKeyTest extends FunSuite {
  test("Can read API key") {
    assert(ApiKey.value != null)
    assert(ApiKey.value.size > 0)
  }
}
