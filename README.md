# Zappy

This library provides limited interfaces to the Zappos REST
API. Queries on the search, product, review, and image verbs are
supported. Not all response fields are supported; a few are
omitted because of limits on the number of fields that case
classes can have in Scala.

The Zappos API has reasonably complete documentation at
[the Zappos API Web site](http://developer.zappos.com/). You
will need an API key (available for free upon request from
Zappos).

# Using the library

There is a class in the `dstu.zappy` package for each of the
supported API search predicates:

* Search: search for products using a search term
* Product: get detailed information for up to 10 products at
  once, addressing them by SKU, UPC, or stock ID
* Review: get reviews for a product, addressing it by SKU
* Image: get URLs of images of a product, addressing it by SKU

The constructors for each of these classes take parameters for
your API key and any other information the API call needs (i.e.,
search terms or product IDs). Other parameters may be set using
their respective `withXXX` or `withoutXXX` methods.

Most actions key off of a product ID. Product IDs are available
in search results. Each API call has a unique response
class. Some response classes contain a large set of results. For
example, `SearchResponse` consists of the API call response
code, search term, information about the number of search
results, and list of a list of `SearchResult`s.

## Example: running a search

Using the API is much the same in Scala and Java. Scala can take
advantage of the fact that most classes are case classes. Be sure
to `import scala.collection.JavaConversions._` to be able to
treat the Java collections classes in the search result objects
as Scala collections.

A simple search application that prints a three-column list of
product ID, product name, and product URL to standard out is
illustrated here:

    :::java
    import dstu.zappy.*;
	
	public class SearchTest {
	  public static void main(String[] args) {
        if (args.length != 3) {
		  System.err.println("Usage: SearchTest <API key> <search term> <maximum number of items to return>");
		  System.exit(-1);
        }
	    String apiKey = args[0];
		String searchTerm = args[1];
		int itemLimit = Integer.parseInt(args[2]);
		Search s = new Search(apiKey, searchTerm).withLimit(itemLimit);
        for (SearchResult r : s.results()) {
		  System.out.println(r.productId() + "\t" + r.productName() + "\t" + r.productUrl());
		}
	  }
	}

If you wanted to, you could use the product ID for each search
term to drill down and get review, image, or other specific
product information for that product with the `Review`, `Image`,
and `Product` classes. See
[the Zappos API documentation](http://developer.zappos.com/) for
definitions of the parameters that these API calls take and the
fields of their response objects.

# Building

Dependency resolution and the project build are managed with
[Maven](http://maven.apache.org/).

## Tests

Builds will automatically run tests that require a Zappos API
key. If you would like to disable these tests, add the argument
`-DskipTests` to all invocations of `mvn`.

If you wish to run these tests yourself:

1. Copy the file `src/test/resources/apikey.properties.template` to `src/test/resources/apikey.properties`
2. Fill in your API key on the appropriate line in `apikey.properties`

## Installing in Maven locally

Run `mvn install` to install the library to your local Maven
repository. Any other projects managed with Maven will be able
to find it.

## Building a standalone jar

If you don't manage your dependencies with Maven, you can still
build a stand-alone jar to drop into your own project. Running
`mvn package` will build jars in `target/`. You can add the
`-jar-with-dependencies.jar` file that is built to your
project's classpath, and you should be able to use Zappy from
there.

# Copyright

All code herein is copyright 2012 Donald S. Black. Use and
redistribution are permitted under the terms of the
[GNU General Public License](http://www.gnu.org/licenses/gpl.html).
